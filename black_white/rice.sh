printf "\n"
printf "This is a rice for i3 with a black and white palette.\n"
printf "\n"

# installing dependencies
sudo pacman -Syu && sudo pacman -S eog thunar ttf-font-awesome python nitrogen rofi alacritty python-pipx playerctl python-dbus python-requests

pipx install bumblebee-status

# setting up nitrogen's wallpaper path relative to the user's home folder
echo "file=/home/$USER/Documentos/wallpapers/wall1.png" >> .config/nitrogen/bg-saved.cfg
echo "dirs=/home/$USER/Documentos/wallpapers;" >> .config/nitrogen/nitrogen.cfg

# copying wallpapers and config files to user's home folder
cp -a wallpapers /home/$USER/Documentos/
cp -a .config/* /home/$USER/.config/

printf "\n"
printf "Ricing finished. Restart the i3 by pressing Command+Shift+R to update the config.\n"
printf "\n"
