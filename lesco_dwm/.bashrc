#
# ~/.bashrc
#

#

PATH=/home/$USER/.local/bin/:$PATH

# sets up terminal colors
cat ~/.cache/wal/sequences 

#If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto -lh -X'
alias grep='grep --color=auto'
alias p='sudo pacman'
alias vim='nvim'

PS1='[\u@\h \W]\$ '

neofetch
