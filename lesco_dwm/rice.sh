#!/bin/bash

dir=$(pwd)

sudo pacman -S --needed git base-devel && git clone https://aur.archlinux.org/yay-bin.git && cd yay-bin/ && makepkg -si 

cd $dir
rm -rf $dir/yay-bin

yay --needed --noconfirm -S python ttf-hack-nerd alacritty man gscreenshot xclip firefox feh dmenu lxappearance papirus-icon-theme materia-gtk-theme xorg-server xorg-xinit xorg-xsetroot xarchiver unzip unrar pcmanfm-gtk3 volumeicon pavucontrol udisks2 gvfs gnome-disk-utility neofetch lxsession-gtk3 picom sxiv python-pywal redshift

git clone https://github.com/Lucolesco/dwm
sudo make -C dwm/ clean install
mv dwm /home/$USER/.config/dwm

git clone https://gitlab.com/dwt1/wallpapers ~/Wallpapers
cp wall.jpg /home/$USER/.config/

cp -r .config/ /home/$USER/
cp .xinitrc /home/$USER/
cp .bashrc /home/$USER/
cp .autostart /home/$USER/
cp .startdwm /home/$USER/
mkdir /home/$USER/.local/
cp -rf bin /home/$USER/.local/bin

