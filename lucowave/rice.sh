#!/bin/bash

dir=$(pwd)

sudo pacman -S --needed git base-devel && git clone https://aur.archlinux.org/yay-bin.git && cd yay-bin/ && makepkg -si 

cd $dir
rm -rf $dir/yay-bin

yay --noconfirm -S python-psutil qtile-extras-git ttf-hack-nerd alacritty zsh man gscreenshot xclip firefox feh dmenu picom lxappearance arc-solid-gtk-theme papirus-icon-theme xarchiver unzip unrar pcmanfm-gtk3 volumeicon pavucontrol lxsession-gtk3 udisks2 gvfs noto-fonts noto-fonts-cjk noto-fonts-emoji noto-fonts-extra gnome-disk-utility

cp -r Wallpapers/ /home/$USER/

cp -r .config/ /home/$USER/

cp .xprofile /home/$USER/

